import numpy as np
import matplotlib.pyplot as plt
from data_reader import DataReader, Normalizer
import functions
import pickle
import pandas as pd

MEAN = 0
STD_DEV = 0.1

class Layer:
    def __init__(self, input_size, output_size, activation_fun, derivative_fun, bias = False):
        self.input_size = input_size
        self.output_size = output_size
        self.fun = activation_fun
        self.df = derivative_fun
        self.bias = bias

        if self.bias:
            self.biases = np.random.normal(MEAN, STD_DEV, output_size)
            self.biases_diff = np.zeros(output_size)
        self.weights = np.random.normal(MEAN, STD_DEV, (input_size, output_size))
        self.weights_diff = np.zeros((input_size, output_size))

    def forward_propagation(self, input):
        self.input = input
        self.input_product = input.dot(self.weights)
        if self.bias:
            self.input_product += self.biases
        self.output = self.fun(self.input_product)

        return self.output

    def backward_propagation(self, output_error, learning_rate, momentum):
        output_error = self.df(self.input_product) * output_error
        input_error = output_error.dot(self.weights.T)
        dWeights = self.input.T.dot(output_error)
        
        self.weights_diff = -learning_rate * dWeights + momentum * self.weights_diff 
        self.weights = self.weights + self.weights_diff

        if self.bias:
            self.biases_diff = -learning_rate * np.sum(output_error, axis = 0,
                keepdims = True) + momentum * self.biases_diff
            self.biases = self.biases + self.biases_diff

        return input_error

    def print_layer(self, precision):
        for i in range(self.weights.shape[1]):
            for j in range(self.weights.shape[0]):
                w_diff = self.weights_diff[j, i]
                w = self.weights[j, i]
                formated_w = f'{" " if w >= 0 else ""}{format(w, f".{precision}f")}'
                formated_w_diff = f'{format(w_diff, f".{precision}f")}'
                print(f"W{i+1}-{j+1}: {formated_w} ({'+' if w_diff >= 0 else ''}{formated_w_diff});", end=" ")
            print("")
        
        if self.bias:
            for i in range(self.biases.shape[1]):
                b = self.biases[0, i]
                b_diff = self.biases_diff[0, i]
                formated_b = f'{" " if b >= 0 else ""}{format(b, f".{precision}f")}'
                formated_b_diff = f'{format(b_diff, f".{precision}f")}'
                print(f"B{i+1}: {formated_b} ({'+' if b_diff >= 0 else ''}{formated_b_diff});", end=" ")
            print("")


class Perceptron:
    def __init__(self, layout , activation_fun, derivative_fun, error_fun, derivative_error, bias = False,
        batch_size = 1, iterations = 1, momentum = 0.2,
        learning_rate = 0.4, seed = 12345, network_type = "regression"):
        
        self.fun = activation_fun
        self.df = derivative_fun
        self.error = error_fun
        self.error_df = derivative_error
        self.layout = layout
        self.bias = bias
        self.batch_size = batch_size
        self.iterations = iterations
        self.momentum = momentum
        self.learning_rate = learning_rate
        np.random.seed(seed)
        self._network_type = network_type
        
        if network_type != 'regression' and network_type != 'classification' :
            raise ValueError("Network type must be regression or classification")

        self._input_size = layout[0]

        self._init_network(layout)
    
    def save(self, epoch, subpath = None):
        if subpath == None:
            with open(f"Perceptron-e{epoch}", "wb") as f:
                pickle.dump(self, f)
        else:
            with open(f"{subpath}Perceptron-e{epoch}", "wb") as f:
                pickle.dump(self, f)

    def _init_network(self, layout):
        self._layers = []

        for i in range(len(layout) - 2):
            self._layers.append(Layer(layout[i], layout[i+1], self.fun, self.df, self.bias)) 

        if self._network_type == 'regression':
            self._layers.append(Layer(layout[-2], layout[-1], functions.do_nth, functions.do_nth_dx, self.bias))
        else:
            self._layers.append(Layer(layout[-2], layout[-1], functions.my_softmax, functions.my_softmax_dx, self.bias))

    def train_digits_with_normalize(self, path, normalizer = None, 
            normalize_as_rgb = True, save_after_epochs = 100, subpath = None,
            should_print_weights = False, print_precision = 4):

        reader = DataReader(path)
        
        reader.push_first_column_to_end()

        if normalize_as_rgb:
            reader.normalize_rgb_apart_from_last(Normalizer(path))
        else:
            reader.normalize_apart_from_last(Normalizer(path))

        error_values = []
        for i in range(self.iterations):
            print(f"\n===================================== Start epoch {i+1}")

            error = self._train_once(reader, should_print_weights, print_precision)
            error_values.append(error)

            print(f"\nverage Error: {error}")
            print(f"===================================== End epoch {i+1}")            
            if (i+1) % save_after_epochs == 0:
                self.save(i+1, subpath=subpath)

        error_values = np.array(error_values)
        iterations = np.array(range(1, self.iterations+1))
        plt.xlabel('Iteration')
        plt.ylabel('Average Error')
        plt.plot(iterations, error_values)
        plt.show()
        plt.clf()

    def train_from_file(self, path, normalizer=None, 
            digit_recognizer = False, save_after_epochs = 100, subpath = None,
            should_print_weights = False, print_precision = 4):

        reader = DataReader(path)
        
        if digit_recognizer:
            reader.push_first_column_to_end()

        if self._network_type == 'regression':
            reader.normalize(normalizer)

        error_values = []
        for i in range(self.iterations):
            print(f"\n===================================== Start epoch {i+1}")

            error = self._train_once(reader, should_print_weights, print_precision)
            error_values.append(error)
    
            print(f"\nAverage Error: {error}")
            print(f"===================================== End epoch {i+1}")
            if (i+1) % save_after_epochs == 0:
                self.save(i+1, subpath=subpath)

        error_values = np.array(error_values)
        iterations = np.array(range(1, self.iterations+1))
        plt.xlabel('Iteration')
        plt.ylabel('Average Error')
        plt.plot(iterations, error_values)
        plt.show()
        plt.clf()
        
    def _train_once(self, reader, should_print_weights, print_precision):
        reader.reset_index()
        reader.shuffle()
        error = 0

        for batch_index, (input_matrix, outcome_matrix) in enumerate(reader.get_records(self.batch_size, self._input_size)):
            for layer in self._layers:
                input_matrix = layer.forward_propagation(input_matrix)

            if self._network_type == 'classification':
                outcome_matrix = self._prepare_outcome(outcome_matrix)

            error += np.sum(self.error(outcome_matrix, input_matrix))
            error_dx = self.error_df(outcome_matrix, input_matrix)

            for layer in reversed(self._layers):
                error_dx = layer.backward_propagation(error_dx, self.learning_rate, self.momentum) 
        
            if should_print_weights:
                print(f"===================== Batch {batch_index + 1}")
                for index, layer in enumerate(self._layers):
                    print(f'======== Layer {index + 1}')
                    layer.print_layer(print_precision)

        error /= reader.get_length()

        return error

    def _prepare_outcome(self, outcome_matrix):
        vectorized_outcome = np.zeros((outcome_matrix.shape[0], self.layout[-1]))
        class_indexes = outcome_matrix - 1
        for col,row_array in enumerate(class_indexes):
            vectorized_outcome[col, row_array[0]] = 1
            
        return vectorized_outcome

    def test_from_file(self, path, normalizer=None):
        reader = DataReader(path)
        if self._network_type == 'regression':
            reader.normalize(normalizer)

        for input_matrix, outcome_matrix in reader.get_records(reader.get_length(), self._input_size):
            calculated_matrix = input_matrix
            for layer in self._layers:
                calculated_matrix = layer.forward_propagation(calculated_matrix)

        self._report_test_result(input_matrix, calculated_matrix, outcome_matrix, normalizer)

    def _report_test_result(self, input_matrix, calculated_matrix, outcome_matrix, normalizer):
        if self._network_type == 'classification':
            calculated_classes = np.argmax(calculated_matrix, axis = 1) + 1
            real_classes = outcome_matrix.flatten() 
            accuracy = np.sum(calculated_classes == real_classes) / input_matrix.shape[0]
            print(f"Classification - Accuracy: {accuracy}")
        else :
            error = np.sum(functions.mean_squared_error(calculated_matrix, outcome_matrix)) ** 1/2

            input_matrix = normalizer.denormalize(input_matrix, 0)
            input_matrix = input_matrix.flatten()
            calculated_matrix = normalizer.denormalize(calculated_matrix, 1)
            calculated_matrix = calculated_matrix.flatten()
            outcome_matrix = normalizer.denormalize(outcome_matrix, 1)
            outcome_matrix = outcome_matrix.flatten()

            plt.plot(input_matrix, calculated_matrix)
            plt.plot(input_matrix, outcome_matrix)
            plt.legend(['Predicted function','Actual function'])
            plt.xlabel('x')
            plt.ylabel('y')
            plt.show()
            plt.clf()

            print(f"Regression - Average Diff: {np.sum(abs(calculated_matrix-outcome_matrix)) / calculated_matrix.shape[0]}")
            print(f"Regression - RMSE: {error}")

    def visualise_classification(self, path):
        reader = DataReader(path)

        for input_matrix, _ in reader.get_records(reader.get_length(), self._input_size):
            calculated_matrix = input_matrix
            
            for layer in self._layers:
                calculated_matrix = layer.forward_propagation(calculated_matrix)
            
            calculated_classes = np.argmax(calculated_matrix, axis = 1) + 1
            plt.scatter(x = input_matrix[:,0], y = input_matrix[:,1], c = calculated_classes,
                marker='x')
            plt.xlabel('x')
            plt.ylabel('y')
            plt.show()
            plt.clf()
    
    def visualise_regression(self, path, normalizer):
        reader = DataReader(path)
        reader.normalize(normalizer)

        # for input_matrix, _ in reader.get_records(reader.get_length(), self._input_size):
        for input_matrix, output in reader.get_records(reader.get_length(), self._input_size):
            calculated_matrix = input_matrix
            for layer in self._layers:
                calculated_matrix = layer.forward_propagation(calculated_matrix)

        input_matrix = normalizer.denormalize(input_matrix, 0)
        input_matrix = input_matrix.flatten()
        # calculated_matrix = normalizer.denormalize(calculated_matrix, 1)
        calculated_matrix = normalizer.denormalize(output, 1)
        calculated_matrix = calculated_matrix.flatten()

        plt.scatter(input_matrix, calculated_matrix,marker = '.')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.show()
        plt.clf()

        
    def test_on_digits(self, digit_path, save_path, normalizer = None, normalize_rgb = False):
        reader = DataReader(digit_path)
        
        if normalize_rgb:
            reader.normalize_rgb_all(Normalizer(digit_path))
        if normalizer:
            normalizer.normalize(reader) # not sure if it works

        for input_matrix, _ in  reader.get_records(reader.get_length(), self._input_size):
            calculated_matrix = input_matrix
            for layer in self._layers:
                calculated_matrix = layer.forward_propagation(calculated_matrix)
            
        calculated_classes = (np.argmax(calculated_matrix, axis = 1) + 1) % 10
        df = pd.DataFrame(
            {
                'ImageId': np.arange(1, reader.get_length() + 1),
                'Label': calculated_classes

            }
        )
        df.to_csv(save_path, index=False)

        return df

def bar_plot(x, y, xlabel = None, ylabel = None, title = None):
    plt.bar(x,y, color = 'blue')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.show()
    plt.clf()

if __name__ == '__main__':

    # pickle_path = '/home/rakusd/Downloads/MGU3Perceptron-e3000'
    # digit_path = 'data/digit-recognizer/test.csv'
    # save_path = 'submission6.csv'

    # with open(pickle_path, 'rb') as f:
    #     perceptron = pickle.load(f)
    
    # perceptron.test_on_digits(digit_path, save_path, normalize_rgb=True)
    # exit()
    # ############## DIGIT RECOGNIZER

    # # perceptron = Perceptron(layout = [784, 1000, 1000, 10], activation_fun = functions.tanh,
    # #     derivative_fun = functions.tanh_dx, error_fun = functions.mean_squared_error,
    # #     derivative_error = functions.mean_squared_error_df, batch_size = 1000, iterations = 30,
    # #     network_type = 'classification', learning_rate=0.2, momentum = 0.2, seed=997909, bias=True)

    # # perceptron.train_from_file('data/digit-recognizer/train.csv', digit_recognizer=True)
    # # exit()

    ############# END DIGIT RECOGNIZER

    perceptron = Perceptron(layout = [1,5,5,1], activation_fun = functions.tanh,
        derivative_fun = functions.tanh_dx, error_fun = functions.mean_absolute_error,
        derivative_error = functions.mean_absolute_error_df, batch_size = 100, iterations = 5000,
        network_type = 'regression', learning_rate=0.0002, momentum = 0.4, seed=907443, bias = True)
    normalizer = Normalizer('data/regression/data.cube.train.1000.csv')
    
    perceptron.train_from_file('data/regression/data.cube.train.1000.csv', normalizer, save_after_epochs=1e6)
    # perceptron.visualise_regression('data/regression/data.activation.train.1000.csv', normalizer)
    perceptron.test_from_file('data/regression/data.cube.test.1000.csv', normalizer)
    exit()
    ############################### VISUALISE
    # perceptron = Perceptron(layout = [2,5,2], activation_fun = functions.tanh,
    #     derivative_fun = functions.tanh_dx, error_fun = functions.cross_entropy,
    #     derivative_error = functions.cross_entropy_df, batch_size = 100, iterations = 100,
    #     network_type = 'classification', learning_rate=0.002, momentum = 0.2, seed=67876, bias=False)
    
    # perceptron.train_from_file('data/classification/data.simple.train.1000.csv',save_after_epochs=1e6)
    # perceptron.visualise_classification('data/classification/data.three_gauss.train.1000.csv')
    # perceptron.test_from_file('data/classification/data.simple.test.1000.csv')
    # exit()

    ############################### VISUALISE
    perceptron = Perceptron(layout = [1,4,4,1], activation_fun = functions.tanh,
        derivative_fun = functions.tanh_dx, error_fun = functions.mean_squared_error,
        derivative_error = functions.mean_squared_error_df, batch_size = 100, iterations = 30,
        network_type = 'regression', learning_rate=0.002, momentum = 0.2, seed=997909, bias=True)
    normalizer = Normalizer('data/regression/data.cube.test.1000.csv')
    perceptron.visualise_regression('data/regression/data.cube.test.1000.csv', normalizer )
    # perceptron.test_from_file('data/classification/data.three_gauss.test.10000.csv')
    exit()

    ### SAVE
    perceptron = Perceptron(layout = [2,4,3,3,3,2], activation_fun = functions.tanh,
        derivative_fun = functions.tanh_dx, error_fun = functions.mean_squared_error,
        derivative_error = functions.mean_squared_error_df, batch_size = 100, iterations = 15,
        network_type = 'classification', learning_rate=0.2, momentum = 0.2, seed=997909, bias=True)
    
    perceptron.train_from_file('data/classification/data.simple.train.10000.csv', save_after_epochs=15)
    
    f = open('Perceptron-e15','rb')
    perceptron2 = pickle.load(f)
    perceptron2.train_from_file('data/classification/data.simple.train.10000.csv')
    perceptron.test_from_file('data/classification/data.simple.test.10000.csv')
    exit()

    # Learning rate too small?
    perceptron = Perceptron(layout = [2,4,4,4,1], activation_fun = functions.tanh,
        derivative_fun = functions.tanh_dx, error_fun = functions.mean_squared_error,
        derivative_error = functions.mean_squared_error_df, batch_size = 20, iterations = 50,
        network_type = 'classification', learning_rate=0.1, momentum = 0.4, seed=125)

    perceptron.train_from_file('data/classification/data.simple.train.1000.csv')
    perceptron.test_from_file('data/classification/data.simple.test.1000.csv')
    exit()

    # With higher learning rate its great now! 
    perceptron = Perceptron(layout = [2,4,2], activation_fun = functions.sigmoid,
        derivative_fun = functions.sigmoid_dx, error_fun = functions.mean_squared_error,
        derivative_error = functions.mean_squared_error_df, batch_size = 20, iterations = 50,
        network_type = 'classification', learning_rate=0.3, momentum = 0.6, seed=125)
    
    perceptron.train_from_file('data/classification/data.simple.train.100.csv')
    perceptron.test_from_file('data/classification/data.simple.test.100.csv')

    