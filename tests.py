from perceptron import Perceptron
from functions import do_nth
import numpy as np

def test_if_works_for_single_value():
    perceptron = Perceptron([2,3,2,2], do_nth, None, None, None, network_type = 'regression')
    
    perceptron._layers[0].weights = np.array([[1,1,2], [2,3,4]])
    perceptron._layers[1].weights = np.array([[2,3], [4,4], [5,2]])
    perceptron._layers[2].weights = np.array([[1,2], [1,3]])

    input_matrix = np.array([[1,1]]) 
    for layer in perceptron._layers:
        input_matrix = layer.forward_propagation(input_matrix)
    expected = np.array([[89, 215]])
    assert (input_matrix == expected).all()

def test_if_works_for_multiple_values():
    perceptron = Perceptron([2,3,2,2], do_nth, None, None, None, network_type='regression')
    perceptron.all_weights = []
    
    perceptron._layers[0].weights = np.array([[1,1,2], [2,3,4]])
    perceptron._layers[1].weights = np.array([[2,3], [4,4], [5,2]])
    perceptron._layers[2].weights = np.array([[1,2], [1,3]])

    input_matrix = np.array([[1,1],[2,2],[3,4]])
    for layer in perceptron._layers:
        input_matrix = layer.forward_propagation(input_matrix)

    expected = np.array([[89, 215], [178, 430], [329, 795]])
    assert (input_matrix == expected).all()

if __name__ == "__main__":
    test_if_works_for_single_value()
    test_if_works_for_multiple_values()