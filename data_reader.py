import pandas as pd


class Normalizer():
    def __init__(self, path, sep = ','):
        self.factors = []

        data_frame = pd.read_csv(path, sep = sep)
        description = data_frame.describe()
        
        for i in range(0, data_frame.shape[1]):
            min = description.iloc[3, i]
            max = description.iloc[7, i]
            self.factors.append((min, max))

    def normalize(self, data_frame):
        for i in range(0, data_frame.shape[1]):
            min, max = self.factors[i]
            data_frame.iloc[:, i] = (1 - -1)/(max - min) * (data_frame.iloc[:, i] - min) + -1
    
    def normalize_apart_from_last(self, data_frame):
        for i in range(0, data_frame.shape[1] - 1):
            min, max = self.factors[i]
            data_frame.iloc[:, i] = (1 - -1)/(max - min) * (data_frame.iloc[:, i] - min) + -1
    
    def normalize_rgb_apart_from_last(self, data_frame):
         for i in range(0, data_frame.shape[1] - 1):
            min, max = 0, 255
            data_frame.iloc[:, i] = (1 - -1)/(max - min) * (data_frame.iloc[:, i] - min) + -1
    
    def normalize_rgb_all(self, data_frame):
         for i in range(0, data_frame.shape[1]):
            min, max = 0, 255
            data_frame.iloc[:, i] = (1 - -1)/(max - min) * (data_frame.iloc[:, i] - min) + -1

    def denormalize(self, x, id):
            min, max = self.factors[id]
            return (max - min)/(1 - -1) * (x - -1) + min


class DataReader():
    def __init__(self, path, sep = ','):
        self._data_frame = pd.read_csv(path, sep = sep)
        self._index = 0

    def push_first_column_to_end(self):
        cols = self._data_frame.columns.tolist()
        cols = cols[1:] + cols[:1]
        self._data_frame = self._data_frame[cols]

    def normalize(self, normalizer):
        normalizer.normalize(self._data_frame)

    def normalize_apart_from_last(self, normalizer):
        normalizer.normalize_apart_from_last(self._data_frame)

    def normalize_rgb_apart_from_last(self, normalizer):
        normalizer.normalize_rgb_apart_from_last(self._data_frame)    

    def normalize_rgb_all(self, normalizer):
        normalizer.normalize_rgb_all(self._data_frame)

    def get_records(self, size, input_size):
        while(self._index < self._data_frame.shape[0]):
            
            # We store values so that we can easily calculate values by multiplying weights matrix by input_matrix
            # input_data = np.array([[x0, y0],[x1, y1], ...])
            input_data = self._data_frame.iloc[self._index:self._index + size,:input_size].values 
            
            # last column is kept as an array
            outcome_array = self._data_frame.iloc[self._index:self._index + size, -1].values
            outcome_array = outcome_array.reshape(outcome_array.shape[0],1)

            # for size = 1, input_data is an array and outcome_array is array with single value
            yield (input_data, outcome_array)

            self._index += size

    def shuffle(self):
        self._data_frame = self._data_frame.sample(frac = 1).reset_index(drop = True)

    def reset_index(self):
        self._index = 0

    def set_index(self, index):
        self._index = index

    def get_length(self):
        return self._data_frame.shape[0]
