import numpy as np
from scipy.special import softmax

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def tanh(x):
    return np.tanh(x)

def relu(x):
    return np.maximum(x, 0)

def leaky_relu(x):
    return np.maximum(0.1*x, x)

def do_nth(x):
    return x

def sigmoid_dx(x):
    return sigmoid(x) * (1 - sigmoid(x))

def tanh_dx(x):
    return 1 - tanh(x) ** 2

# booleans are treated as ints in calculations
def relu_dx(x):
    return x > 0

def leaky_relu_dx(x):
    return np.where(x > 0, 1, 0.1)

def do_nth_dx(x):
   return np.ones(x.shape)

def my_softmax(x):
    return softmax(x, axis = 1)

def my_softmax_dx(x):
    res = softmax(x, axis = 1)
    exponential = np.exp(x)
    exponential_sum = np.sum(exponential, axis = 1, keepdims = True)
    res /= exponential_sum
    res *= exponential_sum - exponential
    return res

def mean_squared_error(x, y):
    return np.sum((y - x) ** 2, axis = 1, keepdims = True) / x.shape[1]

def mean_squared_error_df(x, y):
    return ( 2*y - 2*x) / x.shape[1]
# TODO: Root_mean_squared_error AND/OR root_mean_squared_error_df is not working
def root_mean_squared_error(x, y):
    return mean_squared_error(x, y) ** 1/2

def root_mean_squared_error_df(x, y):
    return 1/2 * 1 / np.maximum((mean_squared_error(x, y) ** 1/2),1e-100) * mean_squared_error_df(x, y)

def cross_entropy(y, x):
    return -np.sum(y * np.log10(x+1e-15) + (1 - y) * np.log10(1-x+1e-15), axis = 1, keepdims = True)

def cross_entropy_df(y, x):
    return -( (y * 1/(x+1e-15)) + ((1-y) * -1/(1-x+1e-15)) )

def mean_absolute_error(x, y):
    return np.mean(np.abs(y - x), axis = 1, keepdims=True)

def mean_absolute_error_df(x, y):
    return np.mean(np.where(y > x, 1, -1), axis = 1, keepdims=True)